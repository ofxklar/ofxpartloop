#include "diamParticle.h"

DiamParticle::DiamParticle() : SimpleParticle(){
  angle = 0;
  accel.set(0, 0);
}

DiamParticle::DiamParticle(ofPoint position, ofPoint vitesse, int size_in) : SimpleParticle(position, vitesse, size_in)
{
  accel.set(0,0);
}

void DiamParticle::draw(){
  float cur_size_loc = cur_size/1.5;
  float cur_size_loc_half = cur_size*2/1.5;

  ofPushStyle();
  ofPushMatrix();
  ofTranslate(pos.x, pos.y);
  ofRotateRad(angle+HALF_PI/2);
  ofSetColor(color, opacity);
  ofFill();
  ofRect(-cur_size_loc, -cur_size_loc, cur_size_loc_half, cur_size_loc_half);
  ofNoFill();
  ofSetColor(0, opacity);
  ofRect(-cur_size_loc, -cur_size_loc, cur_size_loc_half, cur_size_loc_half);
  ofPopMatrix();
  ofPopStyle();
}

DiamParticle::~DiamParticle(){
}
