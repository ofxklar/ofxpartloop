#ifndef NODE_MANAGER_H 
#define NODE_MANAGER_H


#include "simpleParticle.h"
#include "boomParticle.h"
#include "diamParticle.h"
#include "lineParticle.h"
#include "ofMain.h"

#include "layout.h"


class NodeManager
{
 public:
  NodeManager();
  virtual ~NodeManager();

  void update();
  void draw();

  void setLifespan(float lifespan_in);
  void setLayoutParam(ofPoint center, float y_min, float y_max);
  void setLayoutStruct(shared_ptr<layout_s> layout_s_in);
  void setLayout(string layout_in);
  void updateLayout();
  void addParticle();
  void addParticle(ofJson data_in);
  void addDiamParticle(ofJson data_in);
  void addLineParticle(ofJson data_in);
  void addBoomParticle(ofJson data_in);
  void setSeqLength(float seq_length, float start);
  void setRange(float note_min, float note_max, float y_min, float y_max);
  void setRangeLines();
  void setRangeSeq();
  void backToLines(float start_line);
  void setVelocity(float vel_in);
  int getNumParticles();

 private:
  vector<shared_ptr<SimpleParticle>> particles;
  string layout;
  float lifespan;
  float vel;
  float note_min;
  float note_max;
  float y_min;
  float y_max;
  ofPoint center; 
  float seq_start;
  float seq_length;
  shared_ptr<layout_s> layout_data;
};

#endif
