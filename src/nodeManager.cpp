#include "nodeManager.h"

NodeManager::NodeManager(){
  lifespan = 10000;
  layout = "lines";
  vel = 100;
}

void NodeManager::update(){

  vector<shared_ptr<SimpleParticle>>::iterator iter = particles.begin();
  for(; iter != particles.end();){
    if ( !(*iter)->isAlive() ){
      iter = particles.erase(iter);
    }else{
      (*iter)->update();
      (*iter)->setAccel(ofPoint(0, 0));
      ++ iter;
    }
  }
}

void NodeManager::updateLayout(){
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    (*iter)->setData();
    ++ iter;
  }

}

void NodeManager::draw(){
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    if ((*iter)->isBoom()){
      (*iter)->draw();
    }
    ++ iter;
  }

  iter = particles.begin();

  for(; iter != particles.end();){
    if (!(*iter)->isBoom()){
      (*iter)->draw();
    }
    ++ iter;
  }
}

void NodeManager::setLayoutParam(ofPoint center_in, float y_min, float y_max){
  center = center_in;
  y_min = y_min;
  y_max = y_max;
}

void NodeManager::setLayout(string layout_in){
  layout = layout_in;
}

void NodeManager::setLayoutStruct(shared_ptr<layout_s> layout_s_in){
  layout_data = layout_s_in;
}

void NodeManager::addParticle(){
  shared_ptr<SimpleParticle> newParticle(new SimpleParticle);
  newParticle->setLayoutStruct(layout_data);
  particles.emplace_back(newParticle);
}

void NodeManager::addParticle(ofJson data_in){

  shared_ptr<SimpleParticle> newParticle(new SimpleParticle);
  newParticle->setLayoutStruct(layout_data);
  newParticle->setData(data_in);
  particles.emplace_back(newParticle);
}

void NodeManager::addDiamParticle(ofJson data_in){
  shared_ptr<DiamParticle> newParticle(new DiamParticle);
  newParticle->setLayoutStruct(layout_data);
  newParticle->setData(data_in);
  particles.emplace_back(newParticle);
}

void NodeManager::addLineParticle(ofJson data_in){
  shared_ptr<LineParticle> newParticle(new LineParticle);
  newParticle->setLayoutStruct(layout_data);
  newParticle->setData(data_in);
  particles.emplace_back(newParticle);
}


void NodeManager::addBoomParticle(ofJson data_in){
  shared_ptr<BoomParticle> newParticle(new BoomParticle);
  newParticle->setLayoutStruct(layout_data);
  newParticle->setColor(200, 200, 200, 255);
  newParticle->setData(data_in);
  particles.emplace_back(newParticle);
}

void NodeManager::setLifespan(float lifespan_in){
  lifespan = lifespan_in;

  vector<shared_ptr<SimpleParticle>>::iterator iter = particles.begin();
  for(; iter != particles.end();){
    (*iter)->setLifespan(lifespan_in);
    ++iter;
  }
}


void NodeManager::setSeqLength(float seq_length_in, float seq_start_in){
  seq_length = seq_length_in;
  seq_start = seq_start_in;
  setRangeSeq();
  vector<shared_ptr<SimpleParticle>>::iterator iter = particles.begin();
  for(; iter != particles.end();){
    (*iter)->setSeqLength(seq_start, seq_length);
    ++iter;
  }
}


void NodeManager::setRange(float note_min_in, float note_max_in, float y_min_in, float y_max_in){
  note_min = note_min_in;
  note_max = note_max_in;
  y_min = y_min_in;
  y_max = y_max_in;
  if (layout == "lines"){
    setRangeLines();
  }else{
    setRangeSeq();
  }
}

void NodeManager::setRangeLines(){
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    ofPoint old_pos = (*iter)->getPosition();
    float note = float((*iter)->data["midinote"]);
    (*iter)->setPosition(ofPoint(old_pos.x, (int)ofMap(note, note_min, note_max, y_max, y_min)));
    ++ iter;
  }
}

void NodeManager::backToLines(float start_line){
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    ofPoint old_pos = (*iter)->getPosition();
    float note = float((*iter)->data["midinote"]);
    float x = start_line + (ofGetElapsedTimef() - float((*iter)->data["timestamp"])) * vel;
    (*iter)->setPosition(ofPoint(x, old_pos.y));
    (*iter)->setVit(ofPoint(vel, 0));
    ++ iter;
  }
}

void NodeManager::setRangeSeq(){
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    float tone = float((*iter)->data["midinote"]);
    float angle = (float((*iter)->data["timestamp"]) - seq_start) * TWO_PI / seq_length - HALF_PI;
    if ((*iter)->isBoom()){
      ofPoint pt1 = ofPoint(center.x, center.y);
      ofPoint pt2 = pt1 + ofPoint((y_max/2) * cos(angle), (y_max/2) * sin(angle));
      (*iter)->setPosition(pt1, pt2);
    }else{
      float rad = ofMap(tone, note_min, note_max, 0, y_max/2);
      float x = center.x + rad * cos(angle);
      float y = center.y + rad * sin(angle);
      (*iter)->setPosition(ofPoint( x, y ));
    }
    (*iter)->setVit(ofPoint( 0, 0 ));
    ++ iter;
  }
}

void NodeManager::setVelocity(float vel_in){
  vel = vel_in;
  std::vector<shared_ptr<SimpleParticle>> ::iterator iter = particles.begin();
  for(; iter != particles.end();){
    (*iter)->setVit(ofPoint(vel_in, 0));
    ++ iter;
  }
}

int NodeManager::getNumParticles(){
  return particles.size();
}

NodeManager::~NodeManager(){
}
