#ifndef BOOM_PARTICLE_H
#define BOOM_PARTICLE_H

#include "simpleParticle.h"
#include "ofMain.h"

class BoomParticle : public SimpleParticle
{
 public:
  BoomParticle();
  BoomParticle(ofPoint pt1, ofPoint pt2, ofPoint vit, float weight, float size);
  virtual ~BoomParticle();
  
  /* void update(); */
  void set(ofPoint pt1, ofPoint pt2, ofPoint vit, float size_in);
  void setPosition(ofPoint pt1, ofPoint pt2);
  void draw();
  void drawLines();
  void drawSeq();

  bool isBoom();

 protected:
};

#endif
