#ifndef SIMPLE_PARTICLE_H
#define SIMPLE_PARTICLE_H

#include "baseParticle.h"
#include "utils.h"
#include "layout.h"
#include "ofMain.h"

class SimpleParticle : public BaseParticle
{
 public:
  SimpleParticle();
  SimpleParticle(ofPoint pos, ofPoint vit, int size);
  ~SimpleParticle();
  
  void update();
  void draw();
  void drawDebug();

  void set(ofPoint pos, ofPoint vit, int size);
  void setData();
  void setData(ofJson data_in);

  bool isAlive() const;
  bool isBoom();

  ofPoint getPosition() const;
  ofPoint getVit() const;
  ofPoint getAccel() const;

  void setLifespan(int life_in);
  void setLayoutStruct(shared_ptr<layout_s> layout_data_in);
  void setPosition();
  void setPosition(ofPoint new_pos);
  void setPosition(ofPoint pt1, ofPoint pt2);
  void setPositionLines();
  void setPositionSeq();
  void setVit(ofPoint vit);
  void setColor();
  void setColor(int r, int g, int b, int alpha);
  void setColor(ofColor col);
  void setColor(int col, float sat);
  void setAccel(ofPoint accel);

  void setSeqLength(float seq_length_in, float seq_start_in);

  ofJson data;

 protected:
  ofPoint pos;
  ofPoint vit;
  ofPoint accel;

  int size;
  float cur_size;
  ofColor color;
  float opacity;
  bool fade_out;

  // env gen
  float atk;
  float rel;
  float sus;

  int birthdate;
  int age;
  int lifespan;
  bool bAlive;

  bool is_boom;


  int date;
  int old_date;
  int step_duration;

  ofPoint pt1;
  ofPoint pt2;

  float seq_length;
  float seq_start;

  float angle;
  float rad;


  shared_ptr<layout_s> layout_data;
};

#endif
