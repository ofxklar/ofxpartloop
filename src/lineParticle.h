#ifndef LINE_PARTICLE_H
#define LINE_PARTICLE_H

#include "simpleParticle.h"
#include "shapes.h"

class LineParticle : public SimpleParticle
{
 public:
  LineParticle(ofPoint position, ofPoint vit, int size);
  LineParticle();
  virtual ~LineParticle();

  void update();
  void draw();
  void drawLines();
  void drawSeq();

 protected:
  float angle2;
};

#endif
