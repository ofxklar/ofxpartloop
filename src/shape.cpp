#include "shapes.h"

void drawRoundLine(ofPoint pt2, ofPoint pt1, float radius){
  ofBeginShape();
  ofVertex(pt1.x, pt1.y+radius);
  ofVertex(pt2.x, pt2.y+radius);

  for (int i = 0; i < 20; i ++){
    float angle = -HALF_PI + i*PI/20;
    ofVertex(pt2.x + radius * cos(-angle),
             pt2.y + radius * sin(-angle));
  }

  ofVertex(pt2.x, pt2.y-radius);
  ofVertex(pt1.x, pt1.y-radius);

  for (int i = 0; i < 20; i ++){
    float angle = HALF_PI + i*PI/20;
    ofVertex(pt1.x + radius * cos(-angle),
             pt1.y + radius * sin(-angle));
  }

  ofEndShape();
}


void drawRoundArc(ofPoint center, float angle1, float angle2, float radius_b, float radius){
  float radius_int = radius_b - radius;
  float radius_ext = radius_b + radius;

  ofPushMatrix();
  ofTranslate(center);

  ofBeginShape();

  for (int i = 0; i < 50; i ++){
    float angle = ofMap(i, 0, 50, angle1, angle2);
    ofVertex(radius_ext * cos(angle),
             radius_ext * sin(angle));
  }


  ofPoint pt = ofPoint(radius_b * cos(angle2), radius_b * sin(angle2));
  for (int i = 0; i < 20; i ++){
    float angle = angle2 + i * PI/20;
    ofVertex(pt.x + radius * cos(angle),
             pt.y + radius * sin(angle));
  }

  for (int i = 0; i < 50; i ++){
    float angle = ofMap(i, 0, 50, angle2, angle1);
    ofVertex(radius_int * cos(angle),
             radius_int * sin(angle));
  }

  pt.set(radius_b * cos(angle1), radius_b * sin(angle1));
  for (int i = 0; i < 20; i ++){
    float angle = PI + angle1 + i * PI/20;
    ofVertex(pt.x + radius * cos(angle),
             pt.y + radius * sin(angle));
  }
  ofEndShape();
  ofPopMatrix();
}
