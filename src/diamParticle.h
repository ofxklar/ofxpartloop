#ifndef DIAM_PARTICLE_H
#define DIAM_PARTICLE_H

#include "simpleParticle.h"

class DiamParticle : public SimpleParticle
{
 public:
  DiamParticle(ofPoint position, ofPoint vit, int size);
  DiamParticle();
  virtual ~DiamParticle();

  void draw();

 protected:
};

#endif
