#pragma once

#include "ofMain.h"
#include "optionparser.h"
#include "ofxMidi.h"
#include "ofxGui.h"
#include "ofxOsc.h"
#include "ofxOscParameterSync.h"

#include "nodeManager.h"

#include "utils.h"
#include "layout.h"

#define PORT 12345
#define SYNC_SEND_PORT 6668
#define SYNC_RECEIVED_PORT 6669

enum optionIndex {UNKNOWN, HELP, FULLSCREEN, NOGUI, VERBOSE, WIDTH};

class testApp : public ofBaseApp, public ofxMidiListener{

 public:

  void setup();
  void update();
  void draw();

  void colorModeChanged(int & val);
  void layoutChanged(bool & ch);
  void noteRangeChanged(float & note_range);
  void velChanged(int & vel_in);
  void seqLengthChanged(int & seq_in);

  void updateOsc();

  void newParticle(int shape, ofJson data = ofJson());

  void drawScale();

  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

  std::vector<option::Option> options;

  ofxOscReceiver   oscReceiver;

  // Midi
  void newMidiMessage(ofxMidiMessage &args);
  ofxMidiIn midiIn;
  ofxMidiOut midiOut;

  // display
  int fixed_width;

  // GUI
  ofxPanel guigui;
  ofParameterGroup parameters;
  ofxOscParameterSync sync;

  ofParameter<bool> draw_gui;
  ofParameter<bool> layout_lines;
  ofParameter<bool> layout_seq;

  ofParameter<bool> mode_fifth;
  ofParameter<bool> mode_grey;

  ofParameter<bool> draw_scale;

  ofParameter<float> note_min;
  ofParameter<float> note_max;

  ofParameter<float> note_min_target;
  ofParameter<float> note_max_target;

  ofParameter<float> size_min;
  ofParameter<float> size_max;

  ofParameter<int> color_mode;

  ofParameter<int> seq_length;
  ofParameter<float> layout_transition; 
  ofParameter<int> num_particles;
  ofParameter<float> fps;

  // Particles

  NodeManager nodes; 

  shared_ptr<layout_s> layout_data;

  int screen_width;
  int screen_height;

  // foxdot
  ofJson synths;

  float last_msg;
  float seq_start;

  // animation
};

