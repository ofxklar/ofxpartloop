
#include "utils.h"

int getNumFifth(float midi_note){
  int midi_note_int = round(midi_note);
  int reste = midi_note_int%12;
  int num_fifth = 0;
  switch(reste){
  case 0:
    // Do
    num_fifth = 0;
    break;
  case 7:
    // Sol
    num_fifth = 1;
    break;
  case 2:
    // Re
    num_fifth = 2;
    break;
  case 9:
    // La
    num_fifth = 3;
    break;
  case 4:
    // Mi
    num_fifth = 4;
    break;
  case 11:
    // Si
    num_fifth = 5;
    break;
  case 6:
    // Fa#
    num_fifth = 6;
    break;
  case 1:
    // Reb
    num_fifth = 7;
    break;
  case 8:
    // Lab
    num_fifth = 8;
    break;
  case 3:
    // Mib
    num_fifth = 9;
    break;
  case 10:
    // Sib
    num_fifth = 10;
    break;
  case 5:
    // Fa
    num_fifth = 11;
    break;
  }
  return num_fifth;
};

float getColorFifth(float midi_note){
  return ofMap(getNumFifth(midi_note), 0, 12, 0, 255);
};
