#ifndef SHAPE_H
#define SHAPE_H

#include "ofMain.h"

void drawRoundLine(ofPoint pt1, ofPoint pt2, float radius);
void drawRoundArc(ofPoint center, float angle1, float angle2, float radius_b, float radius);

#endif
