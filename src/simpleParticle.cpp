#include "simpleParticle.h"

SimpleParticle::SimpleParticle(): BaseParticle(){
  pos.set(0,0);
  vit.set(0,0);
  accel.set(0,0);

  size = 0;
  cur_size = 0;
  color.set(255,255,255);

  lifespan = 10000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;

  fade_out = true;
  opacity = 255;

  atk = 150;
  rel = 300;
  sus = 0.7;

  is_boom = false;
}

SimpleParticle::SimpleParticle(ofPoint position, ofPoint vitesse, int size_in): BaseParticle(){
  pos = position;
  vit = vitesse;
  accel.set(0,0);

  size = size_in;
  color.set(255,255,255);

  lifespan = 10000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;
}

void SimpleParticle::set(ofPoint position, ofPoint vitesse, int size_in){
  pos = position;
  vit = vitesse;

  size = size_in;
}

void SimpleParticle::setData(){
  lifespan = layout_data->seq_length * 1000;
  if (data["delay"] > 0){
    age = -float(data["delay"]) * 500;
    birthdate += float(data["delay"]) * 500;
  }
  size = ofMap(float(data["amp"]), 0, 1, layout_data->size_min, layout_data->size_max)/2;
  setPosition();
  setColor();
}

void SimpleParticle::setData(ofJson data_in){
  data = data_in;
  setData();
}

void SimpleParticle::setPosition(){
  if (layout_data->layout == "lines"){
    setPositionLines();
  }else if (layout_data->layout == "seq"){
    setPositionSeq();
  }
}

void SimpleParticle::setPositionLines(){
  angle = 0;
  float x = ofMap(age, 0, lifespan, layout_data->center.x, layout_data->width);
  float y = ofMap(float(data["midinote"]),
                   layout_data->note_min,
                   layout_data->note_max,
                   layout_data->y_max,
                   layout_data->y_min);
  pos.set(x, y);
}

void SimpleParticle::setPositionSeq(){
  angle = (float(data["timestamp"]) + (float(data["delay"]) * 0.500) - layout_data->seq_start) * TWO_PI / layout_data->seq_length - HALF_PI;
  rad = ofMap(float(data["midinote"]),
              layout_data->note_min,
              layout_data->note_max,
              layout_data->y_min,
              layout_data->y_max/2);
  float x = layout_data->center.x + rad * cos(angle);
  float y = layout_data->center.y + rad * sin(angle);
  pos.set(x, y);
}

void SimpleParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;

  // vit += accel * step_duration / 1000.0;
  // pos += vit * step_duration / 1000.0;

  if (layout_data->layout == "lines"){
    pos.x = layout_data->center.x + age * layout_data->width / lifespan;
  }

  if (pos.x > layout_data->width){
    pos.x -= layout_data->width;
  }

  bAlive = isAlive();
  if (fade_out){
    opacity = ofMap(age, 0, lifespan, 255, 0);
  }

  if (age>0){
    if (age < atk){
      cur_size = ofMap(age, 0, atk, 0, size);
    }else if( age< atk+rel){
      cur_size = ofMap(age, atk, atk+rel, size, size*sus);
    }else{
      cur_size = size*sus;
    }
  }
}

void SimpleParticle::draw(){
  ofPushStyle();
  ofSetColor(color, opacity);
  ofFill();
  ofDrawCircle(pos.x, pos.y, cur_size);
  // ofDrawCircle(pos.x - layout_data->width, pos.y, cur_size);
  ofNoFill();
  ofSetColor(0, opacity);
  // ofSetColor(0,0,0);
  ofDrawCircle(pos.x, pos.y, cur_size);
  // ofDrawCircle(pos.x - layout_data->width, pos.y, cur_size);
  // ofSetColor(255,255,255);
  ofPopStyle();
}

void SimpleParticle::drawDebug(){
  draw();
  ofPushStyle();
  ofSetColor(0, 255, 0);
  ofDrawLine(pos.x, pos.y, pos.x + vit.x, pos.y + vit.y);
  ofSetColor(0, 0, 255);
  ofDrawLine(pos.x, pos.y, pos.x + accel.x, pos.y + accel.y);
  ofPopStyle();
}

void SimpleParticle::setLifespan(int life_in){
  lifespan = life_in;
}

bool SimpleParticle::isBoom()
{
  return is_boom;
}

bool SimpleParticle::isAlive() const
{
  if (age < lifespan){
    return true;
  }else{
    return false;
  }
}

ofPoint SimpleParticle::getPosition() const
{
  return pos;
}

ofPoint SimpleParticle::getVit() const
{
  return vit;
}

ofPoint SimpleParticle::getAccel() const
{
  return accel;
}

void SimpleParticle::setLayoutStruct(shared_ptr<layout_s> layout_data_in){
  layout_data = layout_data_in;
}

void SimpleParticle::setPosition(ofPoint pt1_in, ofPoint pt2_in){
  pt1 = pt1_in;
  pt2 = pt2_in;
}


void SimpleParticle::setPosition(ofPoint new_pos){
  pos = new_pos;
}

void SimpleParticle::setVit(ofPoint vitesse){
  vit = vitesse;
}

void SimpleParticle::setColor(){
  if (data.find("col") != data.end()) {
    color.setHex(data.at("col"));
  }else{
    std::string synth_name = ofToString(data.at("synth_name"));// + ofToString(data.at("bpf"));
    if (layout_data->color_mode == PL_BY_NOTE){
      color.setHsb((int)getColorFifth(float(data["midinote"])), 255, 255, 255);
    }else if (layout_data->color_mode == PL_BY_SYNTH){
      if (!layout_data->color_synth.contains(synth_name)){
        ofLogNotice() << layout_data->color_synth.size();
        int new_col = (int)getColorFifth(layout_data->color_synth.size()*7);
        layout_data->color_synth[synth_name] = new_col;
      }
      color.setHsb(int(layout_data->color_synth[synth_name]), 255, 255, 255);
    }
  }
}

void SimpleParticle::setColor(int r, int g, int b, int alpha){
  color.set(r, g, b, alpha);
}

void SimpleParticle::setColor(ofColor col){
  color = col;
}

void SimpleParticle::setColor(int col, float sat){
  color.setHsb(col, 255, 255, sat*255);
}

void SimpleParticle::setAccel(ofPoint accel_in){
  accel = accel_in;
}

void SimpleParticle::setSeqLength(float seq_length_in, float seq_start_in){
  seq_length = seq_length_in;
  seq_start = seq_start_in;
}

SimpleParticle::~SimpleParticle(){

}
