#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
  //ofSetFrameRate(60); 
  ofSetWindowTitle("ofxpartloop");

  if (options[VERBOSE]){
    ofSetLogLevel(OF_LOG_VERBOSE);
  }else{
    ofSetLogLevel(OF_LOG_NOTICE);
  }

  ofSetCircleResolution(50);

 // midi
  midiIn.openPort(1);
  // midi.addListener(midiIn.newMessageEvent, this, &testApp::newMessage);

  midiIn.setVerbose(true);
  midiIn.addListener(this);

  // osc
  ofLogNotice() << "Listening for osc messages on port " << PORT;
  oscReceiver.setup(PORT);

  ofLogNotice() << "Listening for param sync on port   " << SYNC_RECEIVED_PORT;
  ofLogNotice() << "Sending param sync on port         " << SYNC_SEND_PORT;

  sync.setup(parameters, SYNC_RECEIVED_PORT, "localhost", SYNC_SEND_PORT);

  ofBackground(10,10,10);

  screen_width = ofGetWidth();
  // screen_width = 1080;
  screen_height = ofGetHeight();

  mode_grey = true;

  // ofLogNotice() << "options";
  // ofLogNotice() << NOGUI;
  // ofLogNotice() << options.size();
  bool mode_gui = true;
  if (options[NOGUI]){
    mode_gui=false;
  }

  fixed_width = 0;
  if (options[WIDTH]){
    std::string ss(options[WIDTH].last()->arg);
    ofLogNotice() << ofToInt(ss);
    fixed_width = ofToInt(ss);
    screen_width = fixed_width;
    // fixed_width = options[WIDTH];
  }
  parameters.setName("parameters");
  parameters.add(draw_gui.set("draw gui (G)", mode_gui));
  parameters.add(layout_lines.set("layout_lines", true));
  parameters.add(layout_seq.set("layout_seq", false));

  parameters.add(note_min_target.set("note_min", 55, 0, 100));
  parameters.add(note_max_target.set("note_max", 60, 0, 150));
  parameters.add(size_min.set("size_min", 0, 0, 100));
  parameters.add(size_max.set("size_max", 50, 0, 100));

  parameters.add(draw_scale.set("draw_scale", true));

  parameters.add(color_mode.set("color_mode", 0, 0, 1));

  // parameters.add(mode_fifth.set("mode_fifth", true));
  parameters.add(seq_length.set("seq length", 8, 0, 128));

  // parameters.add(layout_transition.set("layout transition", 0, 0, 1.0));

  parameters.add(num_particles.set("n particles", 0, 0, 1000));
  parameters.add(fps.set("fps", 60, 0, 60));
  guigui.setup(parameters);

  note_min = note_min_target;
  note_max = note_max_target;

  shared_ptr<layout_s> layout_d(new layout_s);

  layout_d->layout = "lines";
  
  size_max = screen_height / abs(note_min - note_max);

  layout_d->color_mode = PL_BY_NOTE;
  layout_d->width = screen_width;
  layout_d->note_min = note_min;
  layout_d->note_max = note_max;
  layout_d->y_max = screen_height;
  layout_d->y_min = 0;
  layout_d->size_min = size_min;
  layout_d->size_max = size_max;
  layout_d->center = ofPoint(screen_width/2, screen_height/2);
  layout_d->seq_length = seq_length;

  note_min.addListener(this, &testApp::noteRangeChanged);
  note_max.addListener(this, &testApp::noteRangeChanged);

  size_min.addListener(this, &testApp::noteRangeChanged);
  size_max.addListener(this, &testApp::noteRangeChanged);

  seq_length.addListener(this, &testApp::seqLengthChanged);

  color_mode.addListener(this, &testApp::colorModeChanged);

  layout_lines.addListener(this, &testApp::layoutChanged);
  layout_seq.addListener(this, &testApp::layoutChanged);

  layout_data = layout_d;

  ofFile file("synths.json");
  if(file.exists()){
    file >> synths;
  }

  last_msg = ofGetElapsedTimef();

  nodes.setLayoutParam(ofPoint(screen_width/2, screen_height/2), 0, screen_height);
  nodes.setLayout("lines");

  nodes.setLayoutStruct(layout_data);


  ofLogNotice() << "End setup";
}

//--------------------------------------------------------------
void testApp::colorModeChanged(int & val){
  ofLogVerbose() << "Color Scale Changed";
  layout_data->color_mode = val;
}

//--------------------------------------------------------------
void testApp::layoutChanged(bool & ch){
  ofLogVerbose() << "Layout Changed" << endl;
  if (layout_lines){
    layout_data->layout = "lines" ;
  }else if (layout_seq){
    layout_data->layout = "seq";
  }
  nodes.updateLayout();
}


//--------------------------------------------------------------
void testApp::noteRangeChanged(float & note_range){
  ofLogVerbose() << "Range changed: " << note_range << endl;

  layout_data->note_min = note_min;
  layout_data->note_max = note_max;

  layout_data->size_min = size_min;
  layout_data->size_max = size_max;

  size_max = screen_height * 2 / abs(note_min - note_max);

  nodes.updateLayout();
}


//--------------------------------------------------------------
void testApp::velChanged(int & vel_in){
  nodes.setVelocity(vel_in);
}


//--------------------------------------------------------------
void testApp::seqLengthChanged(int & seq_in){
  layout_data->seq_length = seq_in;
  nodes.updateLayout();
}


//--------------------------------------------------------------
void testApp::updateOsc(){

  while( oscReceiver.hasWaitingMessages() ){
    ofxOscMessage m;
    oscReceiver.getNextMessage( m );
    ofLogVerbose() << "Get message " << m;

    if (m.getAddress() == "/s_new"){
      float now = ofGetElapsedTimef();
      if (synths.find(m.getArgAsString(0)) != synths.end()){
        string synth_name = m.getArgAsString(0);
        if (now - last_msg > 4){
          ofLogNotice() << "first message";
          seq_start = now;
          layout_data->seq_start = seq_start;
        }
        last_msg = now;

        ofJson osc_args;
        int num_args = m.getNumArgs();
        for (int i = 4; i < num_args;i+=2){
          osc_args[m.getArgAsString(i)] = m.getArgAsFloat(i+1);
        }

        if(synth_name.rfind("bass") != -1){
          osc_args["midinote"] = float(osc_args["midinote"]) - 12;
        }
        osc_args["synth_name"] = synth_name;
        osc_args["timestamp"] = now;

        if (synths[synth_name]["player"] != "buffer"){
          float note = float(osc_args["midinote"]);
          if (note+1 > note_max_target){
            note_max_target = note+2;
          }else if(note-5 < note_min_target){
            note_min_target = note-7;
          }
        }

        int shape = 0;
        if (synths[synth_name]["player"]=="buffer"){
          shape = 10;
        }else if (synths[synth_name]["type"]=="diam"){
          shape = 1;
        }else if (synths[synth_name]["type"]=="line"){
          shape = 2;
        }

        newParticle(shape, osc_args);
      }
    } else if (m.getAddress() == "/pitch"){
      ofJson osc_args;
      float note = m.getArgAsFloat(0);
      osc_args["synth_name"] = "blip";
      osc_args["timestamp"] = ofGetElapsedTimef();
      osc_args["midinote"] = note;
      osc_args["amp"] = m.getArgAsFloat(1);
      osc_args["delay"] =0; 
      newParticle(0, osc_args);
      if (note+1 > note_max_target){
        note_max_target = note+2;
      }else if(note-5 < note_min_target){
        note_min_target = note-7;
      }
    }
  }
}


//--------------------------------------------------------------
void testApp::newParticle(int shape, ofJson data){
  if (shape == 0){
    nodes.addParticle(data);
  }else if (shape == 1){
    nodes.addDiamParticle(data);
  }else if (shape == 2){
    nodes.addLineParticle(data);
  }else if (shape == 10){
    nodes.addBoomParticle(data);
  }
}


//--------------------------------------------------------------
void testApp::update(){
  ofLogVerbose() << "update: " << ofGetElapsedTimef() << endl;
  fps = ofGetFrameRate();
  num_particles = nodes.getNumParticles();

  sync.update();
  updateOsc();

  float speed_anim = 4;

  if (abs(note_min_target - note_min) > 0.005){
    note_min = note_min + (note_min_target - note_min)/speed_anim;
  }else{
    note_min = note_min_target;
  }

  if (abs(note_max_target - note_max) > 0.005){
    note_max = note_max + (note_max_target - note_max)/speed_anim;
  }else{
    note_max = note_max_target;
  }

  nodes.update();

  if (ofGetFrameNum() == 60){
    oscReceiver.setup(PORT);
  }
}


//--------------------------------------------------------------
void testApp::draw(){
  if (draw_scale){
    drawScale();
  }

  nodes.draw();

  ofPushStyle();
  if (draw_gui) {guigui.draw();}
  ofPopStyle();
}


//--------------------------------------------------------------
void testApp::drawScale(){
  string nom_note;
  for (int i = -note_min; i < note_max; i++){
    bool white_note = true;
    nom_note = "";
    int loc_i = i;
    while(loc_i < 0){
      loc_i += 12;
    }
    int reste = loc_i%12;
    ofSetColor(50,50,50);
    switch(reste){
    case 1:
    case 3: 
    case 6: 
    case 8: 
    case 10:
      {
        ofSetColor(25,25,25);
        white_note = false;
        break;
      }
    case 0:
      nom_note = "Do";
      break;
    case 2:
      nom_note = "Re";
      break;
    case 4:
      nom_note = "Mi";
      break;
    case 5:
      nom_note = "Fa";
      break;
    case 7:
      nom_note = "Sol";
      break;
    case 9:
      nom_note = "La";
      break;
    case 11:
      nom_note = "Si";
      break;
    }
    if (layout_data->layout == "lines"){
      int yy = ofMap(i, note_min, note_max, screen_height, 0);

      if (layout_transition == 0){
        ofDrawBitmapString(nom_note, 10, yy-2);
        ofDrawLine(0, yy, screen_width, yy);
      }else if (layout_transition < 0.5){
        float yy2 = yy/(1 + layout_transition*2);
        float transp = ofMap(layout_transition, 0, 0.5, 255, 0);
        if (white_note){
          ofSetColor(50, 50, 50, transp);
        }else{
          ofSetColor(25, 25, 25, transp);
        }
        ofDrawBitmapString(nom_note, 10, yy2-2);
        if (white_note){
          ofSetColor(50, 50, 50, 255);
        }else{
          ofSetColor(25, 25, 25, 255);
        }
        ofDrawLine(0, yy2, screen_width, yy2);
      }else{
        float t = glm::pow(((double)layout_transition-0.5)*2, 0.1);
        float c_y = ofMap(t, 0, 1, 40000, screen_height/2);
        if (c_y - yy / 2 > 0){
          float rad = c_y - yy/2;
          ofNoFill();
          ofDrawCircle(screen_width/2, c_y, rad);
        }
      }
    }else if (layout_data->layout == "seq"){
      float rr = ofMap(i, note_min, note_max, 0, screen_height/2);
      ofNoFill();
      if (rr > 0 && rr < screen_height/2){
      ofDrawCircle(screen_width/2, screen_height/2, rr);
      }
    }
  }
}


//--------------------------------------------------------------
void testApp::keyPressed  (int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    if (draw_gui){
      draw_gui = false;
    }else{
      draw_gui = true;
    }
    break;

  }

}


//--------------------------------------------------------------
void testApp::keyReleased  (int key){

}


//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}


//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}


//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}


//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}


//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
  if (fixed_width > 0){
    w = fixed_width;
  }
  screen_width = w;
  screen_height = h;

  layout_data->y_max = h;
  layout_data->width = w;
  layout_data->center = ofPoint(w/2, h/2);

  nodes.updateLayout();
}


//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}


//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}


//--------------------------------------------------------------
void testApp::newMidiMessage(ofxMidiMessage &message){
  if (message.status == MIDI_NOTE_ON || message.status == MIDI_NOTE_OFF){
    stringstream midi_log;
    midi_log << message.channel
             << " status: " << message.status
             << " velocity: " << message.velocity
             << " pitch: " << message.pitch;
    ofLogNotice() << "event -   " << midi_log.str();

    if (message.velocity > 0){
      ofJson args;
      args["synth_name"] = "midi-" + message.channel;
      args["timestamp"] = ofGetElapsedTimef();
      args["midinote"] = message.pitch;
      args["amp"] = message.velocity/127.f;
      args["delay"] =0;
      newParticle(0, args);
    }
  }
}
