#pragma once

#include "ofMain.h"

#define PL_BY_NOTE 0
#define PL_BY_SYNTH 1

struct layout_s{
  string layout = "lines";
  int color_mode = PL_BY_SYNTH;
  float width;
  int note_min;
  int note_max;
  float y_min;
  float y_max;
  float size_min;
  float size_max;
  ofPoint center;
  float seq_length;
  float seq_start;
  ofJson color_synth;
};
