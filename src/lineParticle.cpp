#include "lineParticle.h"

LineParticle::LineParticle() : SimpleParticle(){
  angle = 0;
  angle2 = 0;
  accel.set(0, 0);
}

LineParticle::LineParticle(ofPoint position, ofPoint vitesse, int size_in) : SimpleParticle(position, vitesse, size_in)
{
  accel.set(0,0);
}

void LineParticle::update(){
  date = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;

  bAlive = isAlive();
  if (fade_out){
    opacity = ofMap(age, 0, lifespan, 255, 0);
  }

  if (layout_data->layout == "lines"){
    pt1.y = pos.y;
    pt2.y = pos.y;

    if (age < float(data["sus"])*1000){
      pt2.x = layout_data->center.x;
    }else{
      pt2.x = layout_data->center.x + (age - float(data["sus"])*1000) * layout_data->width / lifespan;
    }

    pt1.x = layout_data->center.x + age * layout_data->width/lifespan;
  }else if (layout_data->layout == "seq"){
    float age_dur = float(data["sus"])*1000;
    if (age < float(data["sus"])*1000){
      age_dur = age;
    }
    angle2 = age_dur * TWO_PI / (layout_data->seq_length*1000);
  }

  if (age < atk){
    cur_size = ofMap(age, 0, atk, 0, size);
  }else if( age< atk+rel){
    cur_size = ofMap(age, atk, atk+rel, size, size*sus);
  }else{
    cur_size = size*sus;
  }

}

void LineParticle::draw(){
  if (layout_data->layout == "lines"){
    drawLines();
  }else if (layout_data->layout == "seq"){
    drawSeq();
  }

}

void LineParticle::drawLines(){
  ofPushStyle();
  ofPushMatrix();
  // ofNoFill();
  // ofSetColor(color*0.5, opacity);
  // drawRoundLine(pt1, pt2, cur_size/4);
  // ofPushMatrix();
  // ofTranslate(-layout_data->width, 0);
  // drawRoundLine(pt1, pt2, cur_size/4);
  // ofPopMatrix();
  ofSetColor(color, opacity);
  ofFill();
  drawRoundLine(pt1, pt2, cur_size/4);
  ofPushMatrix();
  ofTranslate(-layout_data->width, 0);
  drawRoundLine(pt1, pt2, cur_size/4);
  ofPopMatrix();
  ofPopMatrix();
  ofPopStyle();
}

void LineParticle::drawSeq(){
  ofPushStyle();
  ofPushMatrix();
  // ofNoFill();
  // ofSetColor(color*0.5, opacity);

  // drawRoundArc(layout_data->center, angle, angle+angle2, rad, cur_size/4);

  ofFill();
  ofSetColor(color, opacity);

  drawRoundArc(layout_data->center, angle, angle+angle2, rad, cur_size/4);
  ofPopMatrix();
  ofPopStyle();
}

LineParticle::~LineParticle(){
}
