#include "boomParticle.h"

BoomParticle::BoomParticle(): SimpleParticle(){
  pt1 = ofPoint(0, 0);
  vit = ofPoint(0, 0);
  size = 0;
  accel = ofPoint(0, 0);
  is_boom = true;
}

BoomParticle::BoomParticle(ofPoint pt1_in, ofPoint pt2_in, ofPoint vitesse, float weight_in, float size_in) : SimpleParticle()
{
  pt1 = pt1_in;
  pt2 = pt2_in;
  vit = vitesse;
  accel.set(0,0);

  size = size_in;
}

void BoomParticle::set(ofPoint pt1_in, ofPoint pt2_in, ofPoint vit_in, float size_in){
  pt1 = pt1_in;
  pt2 = pt2_in;
  vit = vit_in;
  size = size_in;
  is_boom = true;
}

void BoomParticle::setPosition(ofPoint pt1_in, ofPoint pt2_in){
  pt1 = pt1_in;
  pt2 = pt2_in;
}

void BoomParticle::draw(){
  ofPushStyle();
  ofSetColor(180, 180, 180, opacity);
  ofFill();
  if (layout_data->layout == "lines"){
    drawLines();
  }else{
    drawSeq();
  }
  ofPopStyle();
}

void BoomParticle::drawSeq(){
  ofPushMatrix();
  ofTranslate(layout_data->center);
  ofRotateRad(angle);
  ofDrawRectangle(0, -cur_size/20, layout_data->y_max/2, cur_size/10);
  ofPopMatrix();
}

void BoomParticle::drawLines(){
  ofDrawRectangle(pos.x - cur_size/20, layout_data->y_min, cur_size/10, layout_data->y_max - layout_data->y_min);
  ofDrawRectangle(pos.x - cur_size/20 - layout_data->width, layout_data->y_min, cur_size/10, layout_data->y_max - layout_data->y_min);
}


bool BoomParticle::isBoom(){
  return is_boom;
}

BoomParticle::~BoomParticle(){

}
