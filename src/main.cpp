#include "ofMain.h"
#include "testApp.h"
#include "optionparser.h"

constexpr option::Descriptor usage[] =
  {
    {UNKNOWN, 0, "", "", option::Arg::None, "Options:\n"},
    {HELP, 0, "h", "help", option::Arg::None, " -h  \t --help  \t Print usage and exit." },
    {FULLSCREEN, 0, "f", "fullscreen", option::Arg::None, " -f  \t --fullscreen  \t"},
    {NOGUI, 0, "G", "no-gui", option::Arg::None, " -G  \t --no-gui \t disable GUI"},
    {VERBOSE, 0, "v", "verbose", option::Arg::None, " -v \t --verbose \t verbose"},
    {WIDTH, 0, "w", "fixed-width", option::Arg::Optional, " -w \t --fixed-width \t fixed width"},
    {0, 0, 0, 0, 0, 0}
  };

//========================================================================
int main(int argc, char *argv[] ){

  argc-=(argc>0); argv+=(argc>0); // skip program name argv[0] if present
  option::Stats  stats(usage, argc, argv);
  vector<option::Option> options(stats.options_max), buffer(stats.buffer_max);
  option::Parser parse(usage, argc, argv, &options[0], &buffer[0]);

  if (parse.error())
    return 1;
  if (options[HELP]) {
    option::printUsage(std::cout, usage);
    return 0;
  }else{

    if (options[FULLSCREEN]){
      ofSetupOpenGL(1024,768, OF_FULLSCREEN);			// <-------- setup the GL context
    }else {
      ofSetupOpenGL(1024,768, OF_WINDOW);			// <-------- setup the GL context
    }

    testApp *app = new testApp();
    // cout << options[3] << endl;
    app->options = options;

    ofRunApp(app);
  }
}
